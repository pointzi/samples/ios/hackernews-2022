#!/bin/sh -x
echo "Build started on $(date)"

rm -Rf ./build/outputs/*
ARTIFAC_USER="ganesh"
#Uncomment the below line and rerun when there update to xcode or swift version
#carthage update --platform iOS
echo "Record git branch and commit hash ========================================"

GIT_VERSION=$(git log -1 --format="%h")
if [ -z "$CI_COMMIT_REF_NAME" ]; then
    GIT_BRANCH=$(git symbolic-ref --short -q HEAD)
else
    GIT_BRANCH=$CI_COMMIT_REF_NAME
fi
BUILD_TIME=$(date)

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion '${GIT_BRANCH}-${GIT_VERSION}-${BUILD_TIME}'" "./HackerNews/Info.plist"


# ------------------- build HackerNews ------------------------
echo "Run XCode to build App ==================================================="

export GEM_HOME=~/.gem
export FL_UNLOCK_KEYCHAIN_PASSWORD=$BUILD_PASSWORD
export FL_UNLOCK_KEYCHAIN_SET_DEFAULT="/Users/hawk/Library/Keychains/login.keychain-db"
bundle install
bundle update fastlane
pod update
bundle exec fastlane beta_release

appcenter distribute release --app Pointzi/HackerNews-2022 --file build/HackerNews.ipa --group "Collaborators"
